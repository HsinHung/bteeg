package BTEEG.NEW;

import java.io.DataInputStream;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.LinkedList;
import java.util.UUID;
import java.util.concurrent.locks.ReentrantLock;

import android.app.Service;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Environment;
import android.os.IBinder;
import android.util.Log;

public class ConnectDATA extends Service{
	private DataInputStream mmInStream;
	private ConnectThread mConnect;
	private ConnectedThread mConnected;
	private Record mRecord ;
	private String mHospital = new String();
	private String mPatient = new String();
	private File sdcard = Environment.getExternalStorageDirectory();
	
	private ReentrantLock arrLock = new ReentrantLock();
	private String ReceiveData_Action = new String("send_DATA");
	private String Start_Record = new String("STARTRECORD");
	private String Start_Upload = new String("UPLOAD");
	private Intent intent = null;
	private Bundle bundle ;
	
	private static UUID MY_UUID = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");
    private int[] SampleRateSet = { 1, 2, 4, 8, 16, 32, 64, 128, 256, 512, 1024, 0, 0, 0, 0, 0 };
    private int[] ChannelSet = { 1, 2, 4, 8, 16, 32, 64, 128, 256, 512, 1024, 0, 0, 0, 0, 0 };
    private static boolean [] RecordFlag = {false,false};
    private boolean previewing = false;
    private boolean Connect = false;
    private boolean Headrecord = false;
    
    private BluetoothDevice Device;
    
    private LinkedList <int []> AllBuf = new LinkedList<int []>();
    private String DATA;
    private LinkedList <String> miniTime = new LinkedList <String>();
    private String oldData = new String();
    private int MinCount = 0;
    private int [] chBuf;
	private IntentFilter mFilter;
    
	@Override
	public IBinder onBind(Intent intent) {
		// TODO Auto-generated method stub
		return null;
	}
    
    @Override
    public void onCreate() {    
        super.onCreate();    
        
        //create broadcastreceiver
		mFilter = new IntentFilter();
		mFilter.addAction("PREVIEW_FLAG");
		this.registerReceiver(PreView, mFilter);
    }
    @Override
	public void onStart (Intent intent, int startId){
		super.onStart(intent, startId);
		
		//get the content in intent
		Device = (BluetoothDevice)intent.getParcelableExtra("KEY_SOCKET");
		mHospital = new String (intent.getExtras().getString("KEY_HOSPI"));
		mPatient = new String (intent.getExtras().getString("KEY_PATIENT"));
		bundle = intent.getExtras();
		RecordFlag = bundle.getBooleanArray("RECORD_FLAG");
		
		//start connect
		mConnect = new ConnectThread(Device);
		mConnect.start();
	}
    @Override
    public void onDestroy(){
    	Log.e("Destory", "Service Destory.");
    	if(Connect){
//    		mConnect.cancel();
    		mConnected.cancel();
    		
    		//release the broadcastreceiver
    		this.unregisterReceiver(PreView);
    		Log.e("Conn Close", "Connect Close.");
    	}
    	super.onDestroy();
    }

    private class ConnectThread extends Thread{
    	private final BluetoothSocket mmSocket;
    	private final BluetoothDevice mmDevice;
		private BluetoothAdapter mBluetoothAdapter;
    	public ConnectThread(BluetoothDevice device){
    		mmDevice = device;
    		BluetoothSocket tmp = null;
    		try{
    			//use UUID for creating the connect socket
				tmp = device.createRfcommSocketToServiceRecord(MY_UUID);
			}catch(IOException e){
				
			}
    		mmSocket = tmp;
    	}
    	
    	public void run(){
    		//mBluetoothAdapter.cancelDiscovery();
    		try{
    			//bluetooth connect
    			mmSocket.connect();	
    			Connect = true;
    		} catch(IOException e){
        		try{
        			mmSocket.close();
        		} catch(IOException e2){

        		}
        		return;
    		}
    		mConnected = new ConnectedThread(mmSocket);
    		mConnected.start();
//    		mSend = new SendDATA();
//    		mSend.start();
    		//manageConnectedSocket(mmSocket);
    	}
    	public void cancel(){
    		try{
				mmSocket.close();
			}catch(IOException e){
				
			}
    	}
    }
    
	private class ConnectedThread extends Thread {
		private final BluetoothSocket mmSocket;
		private boolean HeaderInfoGet = false;
		private String FilePath;
		private long Ctime;
		private SharedPreferences RecordTime = getSharedPreferences("RECORD_TIME", 0);
		
		public ConnectedThread(BluetoothSocket socket){
		//public ConnectedThread(){
			mmSocket = socket;
			DataInputStream tmpIn = null;
			
			try {
				//get the streamdata
				tmpIn = new DataInputStream(mmSocket.getInputStream());
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			mmInStream = tmpIn;
		}
		
		public void run(){
			String Timeold = null;
			String Timenow = null;
			String SecTime = null;
			SimpleDateFormat getTime;
			int nolose ,info ,SampleRate;
			int Channel = 0;
			long time;
			
			Bundle mbundle;
			Intent mintent;
			Intent STARTUPLOAD;
			
			FilePath = new String(mHospital +"/"+ mPatient);
			
			//sent intent to start preview video
			Intent STARTVIDEO = new Intent(Start_Record);
			sendBroadcast(STARTVIDEO);
			
			while(HeaderInfoGet == false ){
				try {
					nolose = mmInStream.readUnsignedByte();
					if(nolose == 255){
						info = mmInStream.readUnsignedByte();
						SampleRate = SampleRateSet[info / 16];
	                    Channel = ChannelSet[info % 16];
	                    
	                    HeaderInfoGet = true;
					}
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			
			
			
			while(HeaderInfoGet == true && Connect == true){
				//get current time
				Ctime = System.currentTimeMillis();
				Calendar mCalendar = Calendar.getInstance();
				mCalendar.setTimeInMillis(Ctime);
				
				//get date , DATA==date here
				SimpleDateFormat getDATA = new SimpleDateFormat("yyyyMMdd");
				DATA = new String(getDATA.format(Ctime));
				
//				if(RecordFlag[0] ){
					//get record hour
					time = System.currentTimeMillis();
					getTime = new SimpleDateFormat("HH");
					Timenow = new String(getTime.format(time));
					
					//get time
					if(!Timenow.equals(Timeold)){
						getTime = new SimpleDateFormat("HHmmss");
						SecTime = new String(getTime.format(Ctime));
						
						RecordTime.edit()
							.putString("SECTIME", SecTime)
							.commit();
					}
//				}
					
				//read 128 data
				for(int j =0; j<128 ; j++){
					try {
						chBuf = new int [ Channel ];
						//read unsignedbyte 0~255
						nolose = mmInStream.readUnsignedByte();
						if(nolose == 255){
							info = mmInStream.readUnsignedByte();
							
							//get the time for each channel
							time = System.currentTimeMillis();
							getTime = new SimpleDateFormat("HH:mm:ss.SSS");
							String tmpTime = new String(getTime.format(time));
							miniTime.add(tmpTime);
							
							//transfor the value to 0~4095
							for(int i = 0; i<chBuf.length ; i++){
								chBuf[i] = (mmInStream.readUnsignedByte()-64)*64 + (mmInStream.readUnsignedByte()-128);
							}
							AllBuf.add(chBuf);
	//							AllBuf.clear();					
						}
					} catch (IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
					}
				}

				if(previewing){
					if(!Timenow.equals(Timeold)){
						//compair time if time changed,
						//create new file for saving
						Timeold = new String(Timenow.trim());
						
						//sent intent for starting Upload
						STARTUPLOAD = new Intent(Start_Upload);
						sendBroadcast(STARTUPLOAD);
						
						Headrecord = false;
					}
					
					if(RecordFlag[0] ){
						if(!Headrecord){
							//write txt header
							mRecord = new Record(DATA ,miniTime, AllBuf , MinCount, SecTime , FilePath);
							mRecord.headwrite(mHospital, mPatient, DATA);
							
							Headrecord = true;
						}
						//start record eeg
						mRecord = new Record(DATA ,miniTime, AllBuf , MinCount, SecTime , FilePath);
						mRecord.run();
					}
				}
				
				//sent record data to draw eeg
				mbundle = new Bundle();
				mintent = new Intent(ReceiveData_Action);
			
				mbundle.putSerializable("KEY_DATA", AllBuf);
				mbundle.putSerializable("KEY_TIME", miniTime);
				mintent.putExtras(mbundle);
				sendBroadcast(mintent);
				
				AllBuf.clear();
				miniTime.clear();
			}
			
		}
		
		public void cancel(){
			try {
				Connect = false;
				mmSocket.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	public BroadcastReceiver PreView = new BroadcastReceiver(){
		@Override
		public void onReceive(Context context, Intent intent) {
			// TODO Auto-generated method stub
			String action = intent.getAction();
			if(action.equals("PREVIEW_FLAG")){
				//get the video previewing or not
				previewing = intent.getExtras().getBoolean("PREVIEW");
			}
		}
	};
}
