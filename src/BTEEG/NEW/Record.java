package BTEEG.NEW;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.LinkedList;

import android.os.Environment;
import android.util.Log;

public class Record {	
	private LinkedList<int []> mAllBuf = new LinkedList<int []>();
	private int [] cBuf;
	private String State = Environment.getExternalStorageState();
	
	private String HospitalFile = new String("Ntpu");
	private String PatientFile = new String("Csie");
	private String DataTXT = null;
	private String oldData = null;
	private LinkedList<String>  mminiTime = null;
	private long time;
	private String tmpTime;
	private int mMinCount = 0;
	
	private String mPath = null;
	private File SDcard = null;
	private File vPath = null;
	private FileWriter vTXT = null;
	private String FilePath;
	
	public Record(String DATE ,LinkedList<String> miniTime ,LinkedList<int[]> AllBuf ,int MinCount ,String SecTime , String Path){
	//public Record(String DATE ,LinkedList<String> miniTime ,LinkedList<int[]> AllBuf){
		DataTXT = new String(DATE);
		mminiTime = (LinkedList<String>)miniTime.clone();
		mAllBuf = (LinkedList<int[]>)AllBuf.clone();
		tmpTime = new String(SecTime);
//		HospitalFile = new String(Hospital);
//		PatientFile = new String(Patient);
		mPath = new String(Path);
		mMinCount = MinCount;
		
		//get sdcard
		if(State.equals(Environment.MEDIA_REMOVED)){
			return ;
		}
		else{
			SDcard = Environment.getExternalStorageDirectory();
			Log.e("tag",Environment.getExternalStorageDirectory().toString());
		}
		Log.e("tag",Boolean.toString(State.equals(Environment.MEDIA_REMOVED)));

		vPath = new File( SDcard.getPath() +"/"+ mPath );

		Log.e("tag",vPath.toString());
		
		if(!vPath.exists())
			vPath.mkdirs();
		
		Log.e("tag",Boolean.toString(vPath.exists()));
		Log.e("tag",Boolean.toString(vPath.mkdir()));
		
		Log.d("EQUAL_before", "**********" + Boolean.toString(DataTXT.equals(oldData)) + "**********");
			
		FilePath = new String(SDcard.getPath() +"/"+ mPath +"/date"+ DataTXT +"_"+ tmpTime +".txt");
	}

	public void run(){		
		try {		
			
			//write eeg data
			vTXT = new FileWriter(FilePath.trim(), true);
			
			for(int i=0; i<mAllBuf.size() ; i++){
				vTXT.write("\n");
				
				cBuf = mAllBuf.get(i);
				vTXT.write(mminiTime.get(i));
				for(int j=0; j<cBuf.length ;j++){
					vTXT.write(String.format("	%4d", cBuf[j]));
				}
				
				vTXT.write("	");
			}
			
			vTXT.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void headwrite(String mHospital, String mPatient, String DATE){
		try {
			vTXT = new FileWriter(FilePath.trim(), true);
			
			vTXT.write("\n");
			vTXT.write(mHospital + "	");
			vTXT.write(mPatient + "	");
			vTXT.write(DATE + "	");
			
			vTXT.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
