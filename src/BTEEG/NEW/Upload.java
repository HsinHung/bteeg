package BTEEG.NEW;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FilenameFilter;
import java.io.IOException;

import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Environment;
import android.os.IBinder;
import android.util.Log;
import android.widget.DatePicker;
import android.widget.TimePicker;

public class Upload extends Service{
	private String vPath = null;
	private String mHospital;
	private String mPatient;
	private String Yesterday;
	private int UpHour;
	private int Upmin;
	private String uriAPI = "http://120.126.145.151/7v2.php";
	private String Start_Upload = new String("UPLOAD");
	
	private UpVideo mUpVideo;
	private UpText mUpText;
	
	private Calendar day;
	private long Timechange;
	private String SecTime;
	private File sdcard = Environment.getExternalStorageDirectory();
	private SharedPreferences RecordTime;
	
	private DatePicker Datepick;
	private TimePicker Timepick;
	private IntentFilter mFilter;
	private int Counter = 0;
	
	@Override
	public IBinder onBind(Intent arg0) {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	public void onCreate(){
		super.onCreate();

		mFilter = new IntentFilter();
		mFilter.addAction(Start_Upload);
		this.registerReceiver(GETTIME, mFilter);
//		Timepick.setCurrentHour(day.)
//		Timepick.setIs24HourView(true);
//		Timepick.setOnTimeChangedListener(this);
	}
	
	@Override
	public void onStart(Intent intent, int startId){
		super.onStart(intent, startId);
		Log.d("UPload Service Start", "Start Service UPload");
		
		mHospital = new String (intent.getExtras().getString("KEY_HOSPI"));
		mPatient = new String (intent.getExtras().getString("KEY_PATIENT"));
		uriAPI = new String (intent.getExtras().getString("CON_URL"));
		RecordTime = getSharedPreferences("RECORD_TIME", 0);
		
//		mUptext = new UpTXT();
//		mUptext.start();
//		mUpVideo = new UpVideo();
//		mUpVideo.start();
		UpHour = 0;
		Upmin = 53;
	}
	
	@Override
	public void onDestroy(){
		this.unregisterReceiver(GETTIME);
		super.onDestroy();
	}
	
	public BroadcastReceiver GETTIME = new BroadcastReceiver(){
		@Override
		public void onReceive(Context context, Intent intent) {
			// TODO Auto-generated method stub
			String action = intent.getAction();
			
			//if receive action,start upload
			if(action.equals(Start_Upload)){		
//				if(Counter == 60){
				//get date of yesterday for uploading data
				day=Calendar.getInstance(); 
				day.add(Calendar.DATE,-1); 
				SimpleDateFormat sdf=new SimpleDateFormat( "yyyyMMdd ");
				Yesterday = new String(sdf.format(day.getTime()));
				
				SecTime = RecordTime.getString("SECTIME", "");
				
//				vPath = new String(sdcard.getPath() +"/"+ mHospital +"/"+ mPatient +"/date"+ Yesterday.trim() + "*");
				vPath = new String(sdcard.getPath() +"/"+ mHospital +"/"+ mPatient.trim() );
				Log.d("vPath", vPath);
				
					mUpText = new UpText();
					mUpText.start();
					mUpVideo = new UpVideo();
					mUpVideo.start();
//				}
			}
		}
	};
	
	//pick the file TXT of yesterday 
	public class TXTFilter implements FilenameFilter{

		public boolean YESTREDAY(String file){  
			return file.contains(Yesterday.trim()) && file.endsWith(".txt");
		}
		
		@Override
		public boolean accept(File dir, String filename) {
			// TODO Auto-generated method stub
			return YESTREDAY(filename);
		} 
		
	}
	
	private class UpText extends Thread {
		String selectedTEXT;
		File UploadTEXT;
		File TEXT;
		String [] list; 
		
		HttpURLConnection conn = null;
		DataOutputStream dos = null;
		DataInputStream inStream = null;
		String lineEnd = "\r\n";
		String twoHyphens = "--";
		String boundary = "*****";
		int bytesRead, bytesAvailable, bufferSize;
		byte[] buffer;
		int maxBufferSize = 1 * 1024 * 1024;

		public UpText(){
			Log.d("selectedTEXT", vPath);
//			TEXT = new File(selectedTEXT);
			TEXT = new File(vPath);
			list = TEXT.list(new TXTFilter());
			for(int i=0;i<list.length; i++){
				Log.d("TXTexist", list[i]);
			}
			
		}
		
		public void run() {	
			for(int i=0;i<list.length; i++){
				selectedTEXT = new String(vPath +"/"+ list[i]);
				UploadTEXT = new File(selectedTEXT);
				
				if(UploadTEXT.exists()){
					try {	
						// ------------------ CLIENT REQUEST
						FileInputStream fileInputStream = new FileInputStream(UploadTEXT);
						// open a URL connection to the Servlet
						URL url = new URL(uriAPI);
						// Open a HTTP connection to the URL
						conn = (HttpURLConnection) url.openConnection();
						// Allow Inputs
						conn.setDoInput(true);
						// Allow Outputs
						conn.setDoOutput(true);
						//use Stream
						conn.setChunkedStreamingMode(maxBufferSize);
						// Don't use a cached copy.
						conn.setUseCaches(false);
						// Use a post method.
		//				conn.setRequestMethod("POST");
						conn.setRequestProperty("Connection", "Keep-Alive");
						conn.setRequestProperty("Content-Type",
								"multipart/form-data;boundary=" + boundary);
			
						/*--*****\r\n  content \r\n \r\n */
						dos = new DataOutputStream(conn.getOutputStream());
						dos.writeBytes(twoHyphens + boundary + lineEnd);
						dos.writeBytes("Content-Disposition: form-data; name=\"uploadtext\";filename=\""
								+ selectedTEXT + "\"" + lineEnd);
						dos.writeBytes(lineEnd);
			
						// create a buffer of maximum size
						bytesAvailable = fileInputStream.available();
						bufferSize = Math.min(bytesAvailable, maxBufferSize);
						buffer = new byte[bufferSize];
						// read file and write it into form...
						bytesRead = fileInputStream.read(buffer, 0, bufferSize);
			
						while (bytesRead > 0) {
							dos.write(buffer, 0, bufferSize);
							bytesAvailable = fileInputStream.available();
							bufferSize = Math.min(bytesAvailable, maxBufferSize);
							bytesRead = fileInputStream.read(buffer, 0, bufferSize);
						}
			
						// send multipart form data necesssary after file data...
						dos.writeBytes(lineEnd);
						dos.writeBytes(twoHyphens + boundary + twoHyphens + lineEnd);
			
						// close streams
						Log.e("Debug", "File is written");
						fileInputStream.close();
						dos.flush();
						dos.close();
					} catch (MalformedURLException ex) {
						Log.e("Debug", "error: " + ex.getMessage(), ex);
					} catch (IOException ioe) {
						Log.e("Debug", "error: " + ioe.getMessage(), ioe);
					}
				
					// ------------------ read the SERVER RESPONSE
					try {
						inStream = new DataInputStream(conn.getInputStream());
						String str;
						while ((str = inStream.readLine()) != null) {
							Log.e("Debug", "Server Response " + str);
						}
						inStream.close();
					} catch (IOException ioex) {
						Log.e("Debug", "error: " + ioex.getMessage(), ioex);
					}
				}
			}
		}
	}

	//pick the file 3gp of yesterday
	public class VIDEOFilter implements FilenameFilter{

		public boolean YESTREDAY(String file){  
			return file.contains(Yesterday.trim()) && file.endsWith(".3gp");
		}
		
		@Override
		public boolean accept(File dir, String filename) {
			// TODO Auto-generated method stub
			return YESTREDAY(filename);
		} 
		
	}
	
	private class UpVideo extends Thread {
		String selectedVIDEO;
		File UploadVIDEO;
		File VIDEO;
		String [] list; 
		
		HttpURLConnection conn = null;
		DataOutputStream dos = null;
		DataInputStream inStream = null;
		String lineEnd = "\r\n";
		String twoHyphens = "--";
		String boundary = "*****";
		int bytesRead, bytesAvailable, bufferSize;
		byte[] buffer;
		int maxBufferSize = 1 * 1024 * 1024;

		public UpVideo(){
//			Log.d("selectedVIDEO", selectedVIDEO);
			VIDEO = new File(vPath);
			list = VIDEO.list(new VIDEOFilter());
			for(int i=0;i<list.length; i++){
				Log.d("VIDEOexist", list[i]);
			}
		}
		
		public void run() {
			for(int i=0;i<list.length; i++){
				selectedVIDEO = new String(vPath +"/"+ list[i]);
				UploadVIDEO = new File(selectedVIDEO);
				
				if(UploadVIDEO.exists()){
					try {
						// ------------------ CLIENT REQUEST
						FileInputStream fileInputStream = new FileInputStream(UploadVIDEO);
						// open a URL connection to the Servlet
						URL url = new URL(uriAPI);
						// Open a HTTP connection to the URL
						conn = (HttpURLConnection) url.openConnection();
						// Allow Inputs
						conn.setDoInput(true);
						// Allow Outputs
						conn.setDoOutput(true);
						//use Stream
						conn.setChunkedStreamingMode(maxBufferSize);
						// Don't use a cached copy.
						conn.setUseCaches(false);
						// Use a post method.
		//				conn.setRequestMethod("POST");
						conn.setRequestProperty("Connection", "Keep-Alive");
						conn.setRequestProperty("Content-Type",
								"multipart/form-data;boundary=" + boundary);
			
						/*--*****\r\n  content \r\n \r\n */
						dos = new DataOutputStream(conn.getOutputStream());
						dos.writeBytes(twoHyphens + boundary + lineEnd);
						dos.writeBytes("Content-Disposition: form-data; name=\"uploadvideo\";filename=\""
								+ selectedVIDEO + "\"" + lineEnd);
						dos.writeBytes(lineEnd);
			
						// create a buffer of maximum size
						bytesAvailable = fileInputStream.available();
						bufferSize = Math.min(bytesAvailable, maxBufferSize);
						buffer = new byte[bufferSize];
						// read file and write it into form...
						bytesRead = fileInputStream.read(buffer, 0, bufferSize);
			
						while (bytesRead > 0) {
							dos.write(buffer, 0, bufferSize);
							bytesAvailable = fileInputStream.available();
							bufferSize = Math.min(bytesAvailable, maxBufferSize);
							bytesRead = fileInputStream.read(buffer, 0, bufferSize);
						}
			
						// send multipart form data necesssary after file data...
						dos.writeBytes(lineEnd);
						dos.writeBytes(twoHyphens + boundary + twoHyphens + lineEnd);
			
						// close streams
						Log.e("Debug", "File is written");
						fileInputStream.close();
						dos.flush();
						dos.close();
					} catch (MalformedURLException ex) {
						Log.e("Debug", "error: " + ex.getMessage(), ex);
					} catch (IOException ioe) {
						Log.e("Debug", "error: " + ioe.getMessage(), ioe);
					}	
		
					// ------------------ read the SERVER RESPONSE
					try {
						inStream = new DataInputStream(conn.getInputStream());
						String str;
						while ((str = inStream.readLine()) != null) {
							Log.e("Debug", "Server Response " + str);
						}
						inStream.close();
					} catch (IOException ioex) {
						Log.e("Debug", "error: " + ioex.getMessage(), ioex);
					}
				}
			}
		}
	}

}
