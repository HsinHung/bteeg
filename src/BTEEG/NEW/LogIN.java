package BTEEG.NEW;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

public class LogIN extends Activity {
    private EditText Hospital;
	private EditText Patient;
	private EditText Password;
	private String mHospital;
	private String mPatient;
	private String mPassword;
	private Bundle mbundle = new Bundle();
	private Intent mintent = new Intent();

	/** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        
        Hospital = (EditText) this.findViewById(R.id.editText1);
        Hospital.setText(R.string.Hospital);
        Patient = (EditText) this.findViewById(R.id.editText2);
        Patient.setText(R.string.Patient);
//        Password = (EditText) this.findViewById(R.id.editText3);
//        Patient.setText(null);
    }
    
    public void record(View v){
    	mHospital = new String(Hospital.getText().toString());
    	mPatient = new String(Patient.getText().toString());
//    	mPassword = new String(Password.getText().toString());
    	
    	//put the value in bundle to the next Activity
    	mbundle.putString("KEY_HOSPI", mHospital);
    	mbundle.putString("KEY_PATIENT", mPatient);
//    	mbundle.putString("KEY_PASS", mPassword);
    	mintent.putExtras(mbundle);
    	
    	mintent.setClass(LogIN.this, BTeegActivity.class);
    	startActivity(mintent);
    }
}
