package BTEEG.NEW;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.LinkedList;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.graphics.PixelFormat;
import android.hardware.Camera;
import android.media.CamcorderProfile;
import android.media.MediaRecorder;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;

public class Video extends Activity implements SurfaceHolder.Callback{
	private Camera mCamera; 
	private MediaRecorder mMediaRecorder;
	private SurfaceView previewSurface;
	private SurfaceHolder previewHolder;
	private Intent intent ;
	private Bundle mbundle;
	private boolean previewing = false;
	private boolean recording = false;
	private boolean camOpen = false;
	private boolean view = false;
	private File SDcard = null;
	private String vPath = null;
	
	private String State = Environment.getExternalStorageState();
	private SharedPreferences RecordTime;
	private static boolean [] RecordFlag = {false,false};
	private String PREVIEWING = new String("PREVIEW_FLAG");
	private String Start_Upload = new String("UPLOAD");
	private int Counter = 0;
	
	private long time;
	private String mHospital = new String("Ntpu");
	private String mPatient = new String("Csie");
	private String DataTXT = null;
	private String tmpTime = null;
	private String SecTime;
	private LinkedList<String>  mminiTime = null;
	private Bundle preViewbundle = new Bundle();
	private Intent preViewintent = new Intent(PREVIEWING);
	private IntentFilter mFilter;
	
	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		previewSurface = new SurfaceView(this);
		setContentView(previewSurface);
		setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
//		previewSurface = (SurfaceView) this.findViewById(R.id.surfaceView2);

		intent = getIntent();
		mbundle = intent.getExtras();
		RecordFlag = mbundle.getBooleanArray("RECORD_FLAG");
		SDcard = Environment.getExternalStorageDirectory();
		RecordTime = getSharedPreferences("RECORD_TIME", 0);
		
		//prepare to preview
		previewHolder = previewSurface.getHolder();
		previewHolder.addCallback(this);
		previewHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
		
		
		mHospital = new String (intent.getExtras().getString("KEY_HOSPI"));
		mPatient = new String (intent.getExtras().getString("KEY_PATIENT"));
		
		//new recorder
		mMediaRecorder = new MediaRecorder();
		
		//set broadcastreceiver
		mFilter = new IntentFilter();
		mFilter.addAction(Start_Upload);
		this.registerReceiver(GETTIME, mFilter);
		Log.e("Video Create", "Video create success");
	}
	
	@Override
	protected void onSaveInstanceState(Bundle outState) {
	}
	
    @Override
    public void onResume(){
    	super.onResume();
    	
    	getWindow().setFormat(PixelFormat.UNKNOWN);
//		previewSurface = (SurfaceView) findViewById(R.id.surfaceView2);
//    	if(view){
//    		try {
//				mCamera.reconnect();
//			} catch (IOException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
//    	}
    	
    	//set the view is visible
		previewSurface.setVisibility(View.VISIBLE);
		view = true;
		Log.e("Video onResume", "Video onResume success");
    }    
    @Override
    public void onPause(){
    	super.onPause();
//    	previewing = false;	
//    	previewSurface.setVisibility(View.GONE);
    	view = false;
    	Log.e("Video Pause", "Video onpause success");
    }
    @Override
    public void onDestroy(){
    	this.unregisterReceiver(GETTIME);
    	RecordStop();
    	PreviewStop();
    	
    	super.onDestroy();
    }
    
	
	@Override
	public void surfaceChanged(SurfaceHolder holder, int format, int width,
			int height) {
		// TODO Auto-generated method stub
		
		
		//Get time and Path
		time = System.currentTimeMillis();
		SimpleDateFormat getDATA = new SimpleDateFormat("yyyyMMdd");
		DataTXT = new String(getDATA.format(time));
		time = System.currentTimeMillis();
		SimpleDateFormat getTime = new SimpleDateFormat("HHmmss");
		tmpTime = new String(getTime.format(time));
		
		SecTime = RecordTime.getString("SECTIME", "");
		
		vPath = new String(SDcard.getPath() +"/"+ mHospital +"/"+ mPatient +"/date"+ DataTXT +"_"+ SecTime +".3gp");
//		vPath = new String(SDcard.getPath() +"/"+ mHospital +"/"+ mPatient +"/date"+ DataTXT +".3gp");
//		if(!recording && RecordFlag[1]){
//			prepareVideoRecorder(vPath);
//			mMediaRecorder.start();
//			recording = true;
//		}
		
		
	}

	@Override
	public void surfaceCreated(SurfaceHolder holder) {
		// TODO Auto-generated method stub
		if(!camOpen){
			mCamera = Camera.open();
			camOpen = true;
		}
		
		if(!previewing){
			try {
				previewing = true;
				
				preViewbundle.putBoolean("PREVIEW", previewing);
				preViewintent.putExtras(preViewbundle);
				
				//sent message to connectdata to start record
				this.sendBroadcast(preViewintent);
				
				//set and start preview
				mCamera.setPreviewDisplay(holder);
				mCamera.startPreview();
				
				
//				preViewbundle = new Bundle();
//				preViewintent = new Intent(PREVIEWING);
				

				
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	@Override
	public void surfaceDestroyed(SurfaceHolder holder) {
		// TODO Auto-generated method stub
		
//		mCamera.stopPreview();
//		mCamera.release();
//		mCamera = null;
//		previewing = false;
	}

	
	private boolean prepareVideoRecorder(String mPath){
	    // Step 1: Unlock and set camera to MediaRecorder
	    mCamera.unlock();
	    mMediaRecorder.setCamera(mCamera);

	    // Step 2: Set sources
	    mMediaRecorder.setAudioSource(MediaRecorder.AudioSource.CAMCORDER);
	    mMediaRecorder.setVideoSource(MediaRecorder.VideoSource.CAMERA);

	    // Step 3: Set a CamcorderProfile (requires API Level 8 or higher)
	    mMediaRecorder.setProfile(CamcorderProfile.get(CamcorderProfile.QUALITY_HIGH));
	    
	    // Step 4: Set output file
	    mMediaRecorder.setOutputFile(mPath.toString());

	    // Step 5: Set the preview output
	    mMediaRecorder.setPreviewDisplay(previewSurface.getHolder().getSurface());

	    // Step 6: Prepare configured MediaRecorder
	    try {
			mMediaRecorder.prepare();
		} catch (IllegalStateException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	    return true;
	}
	
	public void RecordStop(){
    	if(recording){
	    	mMediaRecorder.stop();
	        mMediaRecorder.reset();   // clear recorder configuration
	        mMediaRecorder.release(); // release the recorder object
	        mMediaRecorder = null;
	        recording = false;
	        
	    	mCamera.lock();
    	}
	}
	public void PreviewStop(){
    	if(previewing){
			mCamera.stopPreview();
			mCamera.release();
			mCamera = null;
			previewing = false;
			
			preViewbundle.putBoolean("PREVIEW", previewing);
			preViewintent.putExtras(preViewbundle);
			this.sendBroadcast(preViewintent);
    	}
	}
	
	
	public BroadcastReceiver GETTIME = new BroadcastReceiver(){
		@Override
		public void onReceive(Context context, Intent intent) {
			// TODO Auto-generated method stub
			String action = intent.getAction();
			if(action.equals(Start_Upload)){
				//if receive action and recordflag == true , start record
				if(RecordFlag[1]){
					RecordStop();
					mMediaRecorder = new MediaRecorder();
					
					//Get time and Path
					time = System.currentTimeMillis();
					SimpleDateFormat getDATA = new SimpleDateFormat("yyyyMMdd");
					DataTXT = new String(getDATA.format(time));
					
					SecTime = RecordTime.getString("SECTIME", "");
					
					//set save path
					vPath = new String(SDcard.getPath() +"/"+ mHospital +"/"+ mPatient +"/date"+ DataTXT +"_"+ SecTime +".3gp");
					
					//start record
					if(!recording && RecordFlag[1]){
						prepareVideoRecorder(vPath);
						mMediaRecorder.start();
						recording = true;
					}
//					Counter = 0;
				}
			}
		}
	};
}
