package BTEEG.NEW;

import java.util.Set;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Toast;
import android.widget.ToggleButton;

public class BTeegActivity extends Activity {
	private BluetoothAdapter mBTAdapter;
	private BluetoothDevice Device = null;
	
	
	private String Device_name = "SPP";
	private static final int REQUEST_ENABLE_BT = 2;
	private static boolean [] ch_on_off = {true,true,true,true,true,true,true,true};
	private static boolean [] RecordFlag = {false,false};
	private boolean serviceStart = false;
	private String ConURL;
	
	private Bundle mbundle ;
	private Intent intent ;
	private Intent intentConn;
	private Intent intentUp;
	private ToggleButton CH_1;
	private ToggleButton CH_2;
	private ToggleButton CH_3;
	private ToggleButton CH_4;
	private ToggleButton CH_5;
	private ToggleButton CH_6;
	private ToggleButton CH_7;
	private ToggleButton CH_8;
	private CheckBox RecordEEG;
	private CheckBox RecordVideo;
	private EditText mURL;
	
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.btsetting);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        
        //set the CH button
        CH_1 = (ToggleButton) this.findViewById(R.id.toggleButton1);
        CH_2 = (ToggleButton) this.findViewById(R.id.toggleButton2);
        CH_3 = (ToggleButton) this.findViewById(R.id.toggleButton3);
        CH_4 = (ToggleButton) this.findViewById(R.id.toggleButton4);
        CH_5 = (ToggleButton) this.findViewById(R.id.toggleButton5);
        CH_6 = (ToggleButton) this.findViewById(R.id.toggleButton6);
        CH_7 = (ToggleButton) this.findViewById(R.id.toggleButton7);
        CH_8 = (ToggleButton) this.findViewById(R.id.toggleButton8);
        RecordEEG = (CheckBox) this.findViewById(R.id.checkBox1);
        RecordVideo = (CheckBox) this.findViewById(R.id.checkBox2);
        mURL = (EditText) this.findViewById(R.id.URLText1);
        mURL.setText(R.string.defaultURL);
  
        //get previous intent
        intent = getIntent();
        mbundle = getIntent().getExtras();
        
        //start bluetooth
		Intent enableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
		startActivityForResult(enableIntent,REQUEST_ENABLE_BT);
    }
    @Override
    public void onDestroy(){
    	if(serviceStart){
	    	stopService(intentConn);
	    	stopService(intentUp);
    	}
    	super.onDestroy();
    }
    
    public void ConnectRecord(View v){
    	//get default bluetooth
    	mBTAdapter = BluetoothAdapter.getDefaultAdapter();
    	//get paired devices
    	Set<BluetoothDevice> pairedDevices = mBTAdapter.getBondedDevices();
    	if(pairedDevices.size() > 0 ){
    		for(BluetoothDevice device : pairedDevices){
    			//choose SPP device
    			if(device.getName().contentEquals(Device_name)){
    				
    				Toast.makeText(this, device.getName(), Toast.LENGTH_LONG).show();
    				Device = mBTAdapter.getRemoteDevice(device.getAddress());
    				//break;
    			}
    		}
    	}
    	
		if(Device == null){
			Toast.makeText(this, "裝置未開啟", Toast.LENGTH_LONG).show();
		}		
		
		ConURL = new String(mURL.getText().toString());
		
		//sent the CH and recordflag and Server URL
		mbundle.putBooleanArray("KEY_CH", ch_on_off);
		mbundle.putBooleanArray("RECORD_FLAG", RecordFlag);
		mbundle.putString("CON_URL", ConURL);
		
		intent.putExtras(mbundle);
		intent.putExtra("KEY_SOCKET", Device);
		
		intentConn = (Intent) intent.clone();
		intentUp = (Intent) intent.clone();
		
		//start Service to connect device
		if(!serviceStart){
			intentConn.setClass(this, ConnectDATA.class);
			startService(intentConn);
			intentUp.setClass(this, Upload.class);
			startService(intentUp);
			Log.d("Service Start", "Start Service");
			serviceStart = true;
		}
		
		//intent.setClass(BTeegActivity.this, EEGDraw.class);
		intent.setClass(BTeegActivity.this, chooseView.class);
		startActivity(intent);
    }
    public void StopRecord(View v){
    	mBTAdapter = BluetoothAdapter.getDefaultAdapter();
    	if(mBTAdapter == null){
    		Toast.makeText(this, "未提供藍芽通訊", Toast.LENGTH_LONG).show();
    		finish();
    	}
    	
    	if(serviceStart){
	    	stopService(intentConn);
	    	stopService(intentUp);
    	}
    	
//    	if(mBTAdapter.isEnabled()){
//    		mBTAdapter.disable();
//    	}
    }
    
    //set channel ON or OFF
    public void CH_1(View v){
    	ch_on_off[0]  = CH_1.isChecked();
    }
    public void CH_2(View v){
    	ch_on_off[1]  = CH_2.isChecked();
    }
    public void CH_3(View v){
    	ch_on_off[2]  = CH_3.isChecked();
    }
    public void CH_4(View v){
    	ch_on_off[3]  = CH_4.isChecked();
    }
    public void CH_5(View v){
    	ch_on_off[4]  = CH_5.isChecked();
    }
    public void CH_6(View v){
    	ch_on_off[5]  = CH_6.isChecked();
    }
    public void CH_7(View v){
    	ch_on_off[6]  = CH_7.isChecked();
    }
    public void CH_8(View v){
    	ch_on_off[7]  = CH_8.isChecked();
    }
    public void EEGrecord(View v){
//    	Toast.makeText(this, "Radio Button EEG click", Toast.LENGTH_SHORT).show();
    	RecordFlag[0] = RecordEEG.isChecked();
    }
    public void Videorecord(View v){
//    	Toast.makeText(this, "Radio Button Video click", Toast.LENGTH_SHORT).show();
    	RecordFlag[1] = RecordVideo.isChecked();
    }
}