package BTEEG.NEW;

import android.app.TabActivity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.widget.TabHost;
import android.widget.TabHost.OnTabChangeListener;
import android.widget.Toast;

public class chooseView extends TabActivity implements OnTabChangeListener{
	private Intent intent;
	private String TAB_1_TAG = new String("Video");
	private String TAB_2_TAG = new String("EEG");
	private TabHost tabHost ;

	private String Start_Record = new String("STARTRECORD");
	private IntentFilter mFilter;
	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.choice);
		setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);

		//get previous intent
		intent = getIntent();

//		intent.setClass(this, ConnectDATA.class);
//		startService(intent);
//		intent.setClass(this, Upload.class);
//		startService(intent);
		
		//create action 
		mFilter = new IntentFilter();
		mFilter.addAction(Start_Record);
		//register broadcastreceiver
		this.registerReceiver(STARTVIDEO, mFilter);
		
		// get tabHost
		tabHost = getTabHost();
		//set listener
		tabHost.setOnTabChangedListener(this);

		//set each tab with View
		intent.setClass(this, Video.class);
		tabHost.addTab(tabHost.newTabSpec(TAB_1_TAG)
				.setIndicator("Video")
				.setContent(intent)
				);		
		intent.setClass(this, EEGDraw.class);
		tabHost.addTab(tabHost.newTabSpec(TAB_2_TAG)
				.setIndicator("EEG")
				.setContent(intent)
				);
//		tabHost.setCurrentTab(0);
		//set the showing view
		tabHost.setCurrentTab(1);
	}
	@Override
	public void onDestroy(){
		this.unregisterReceiver(STARTVIDEO);
		
		super.onDestroy();
//		tabHost.clearAllTabs();
	}

	@Override
	public void onTabChanged(String tabId) {
		// TODO Auto-generated method stub
		//change the showing view
	    if(tabId.equals(TAB_1_TAG)) {
	    	tabHost.setCurrentTab(0);
	    }
	    else if(tabId.equals(TAB_2_TAG)) {
	    	tabHost.setCurrentTab(1);
	    }
	    
    	Toast toast=Toast.makeText(getApplicationContext(), tabId, Toast.LENGTH_SHORT);  
        toast.show();
	}
	
	//new a broadcastreceiver
	BroadcastReceiver STARTVIDEO = new BroadcastReceiver(){

		@Override
		public void onReceive(Context context, Intent intent) {
			// TODO Auto-generated method stub
			String action = intent.getAction();
			
			//if connect =>start recordvideo
			if(action.equals(Start_Record)){
				tabHost = getTabHost();
				tabHost.setCurrentTab(0);
			}
		}
		
	};
}
