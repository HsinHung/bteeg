package BTEEG.NEW;

import java.util.Collection;
import java.util.LinkedList;
import java.util.Timer;
import java.util.TimerTask;

import android.app.Activity;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ActivityInfo;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.os.Bundle;
import android.util.Log;
import android.view.SurfaceView;
import android.view.View;

public class EEGDraw extends Activity {
    public final String ReceiveData_Action = new String("send_DATA");
	private SurfaceView drawS;
	private Timer mTimer;    
	private MyTimerTask mTimerTask;        
	private BluetoothDevice device;
	private Intent intent ;
	private Bundle mbundle;
	private IntentFilter mFilter;
	
	private LinkedList<int []> DATABuf = new LinkedList<int []>();
	private LinkedList <int []> tmpBuf = new LinkedList<int []>();
	private boolean [] ch_on_off = {true,true,true,true,true,true,true,true};
	private static LinkedList <String> mminiTime = new LinkedList<String>();
	
	private int centerY ,startY;
	private int p_distanceY = 42;
	private int num = 100 ,oldX ,X ,oldY ,Y;
	
	private int [] oldBuf;
	private int [] Buf  = null;
	private static int[] Y_axis;
	//private Intent intent;
	/** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        drawS = new SurfaceView(this);
//        setContentView(drawS);
//        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        setContentView(R.layout.drawing);
        drawS = (SurfaceView) this.findViewById(R.id.surfaceView1);

//        centerY = (getWindowManager().getDefaultDisplay().getHeight() - drawS.getTop()) / 8; 
        Y_axis = new int[getWindowManager().getDefaultDisplay().getWidth()];
        
        mTimer = new Timer();
        intent = getIntent();
        mbundle = getIntent().getExtras();
        
        ch_on_off = mbundle.getBooleanArray("KEY_CH");
        oldY = centerY;
        
        mFilter = new IntentFilter(ReceiveData_Action);
        
        
        mTimerTask = new MyTimerTask();
        mTimer.schedule(mTimerTask, 0, 1); 
//        mTimerTask = new MyTimerTask(); 
//		intent.setClass(EEGDraw.this, ConnectDATA.class);
//		startService(intent);
    }
    @Override
    public void onResume(){
    	super.onResume();
    	
    	setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
//    	drawS = (SurfaceView) this.findViewById(R.id.surfaceView1);
    	drawS.setVisibility(View.VISIBLE);
        centerY = (getWindowManager().getDefaultDisplay().getHeight() - drawS.getTop()) / 8; 
        this.registerReceiver(mReceiver, mFilter);
    }    
    @Override
    public void onPause(){
    	super.onPause();

//    	mTimer.purge();
//    	mTimerTask.cancel();
    	Log.e("Pause", "Pause success" );
    	this.unregisterReceiver(mReceiver);
    	DATABuf.clear();
    	drawS.setVisibility(View.GONE);
//    	ClearDraw();
    	X = 0;    
        oldY = centerY;  
    }
    @Override
    public void onStop(){
    	Log.e("Stop", "Stop success" );
    	super.onStop();
    }
    @Override
    public void onDestroy(){
    	Log.e("Destroy", "Destroy success" );
    	
    	super.onDestroy();
    	
    }
    
    public BroadcastReceiver mReceiver = new BroadcastReceiver(){

		@Override
		public void onReceive(Context context, Intent intent) {
			// TODO Auto-generated method stub
			String action = intent.getAction();
			if(action.equals(ReceiveData_Action)){
				tmpBuf = new LinkedList<int []>((Collection<? extends int[]>) intent.getExtras().getSerializable("KEY_DATA"));
				mminiTime = new LinkedList<String>((Collection<? extends String>) intent.getExtras().getSerializable("KEY_TIME"));
				
				if(tmpBuf.size() != 0){
					DATABuf.addAll(tmpBuf);
					tmpBuf.clear();
				}
			}
		}
    	
    };
    
    class MyTimerTask extends TimerTask {    
    	public void run() {    
    		if(DATABuf.size() >= 5 ){
//    			for(int i=0; i == 50; i++){
//    			while(DATABuf.size()>=3){
    			    SDraw(X);
    			    X ++;   
    			    if (X == Y_axis.length - 1) {     
    			        ClearDraw();    
    			        X = 0;    
    			        oldY = centerY;    
    			    }    
//    			    Log.d("TIMER","GOGOGO");
//    			}
//    			}

    		}
    	}     
    }    
    
    private void SDraw(int length){
    	startY = 5;
	    if (length == 0)    
	        oldX = 0; 
	     
	    oldBuf = DATABuf.get(0);
	    Buf = DATABuf.get(2);
	    DATABuf.removeFirst();
	    DATABuf.removeFirst();
	     
	    Canvas canvas = drawS.getHolder().lockCanvas(new Rect(oldX, 0, oldX + length,    
	            getWindowManager().getDefaultDisplay().getHeight()));  
	     
	    Paint paint = new Paint();    
	    paint.setColor(Color.GREEN);     
	    paint.setStrokeWidth(1);
	     
//	    while(DATABuf.size()>=3){

		     
		     
		    for(int i=0; i <Buf.length; i++){
		    	if(ch_on_off[i] == true)
		    		canvas.drawLine(oldX, oldBuf[i]/num +p_distanceY*i + startY, length, Buf[i]/num +p_distanceY*i + startY, paint);
		    }
	//	    canvas.drawLine(oldX, oldBuf[0]/num +p_distanceY*0, length, Buf[0]/num +p_distanceY*0, paint);
		    oldX = length;
//	    }
	    drawS.getHolder().unlockCanvasAndPost(canvas);
    }

	void ClearDraw() {    
		  Canvas canvas = drawS.getHolder().lockCanvas(null);    
		  canvas.drawColor(Color.BLACK);     
		  drawS.getHolder().unlockCanvasAndPost(canvas);    
	}   
}
